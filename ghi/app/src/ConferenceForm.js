import React, {useEffect, useState} from 'react';

function ConferenceForm () {
    const [name, setName] = useState('');
    const [locations, setLocations] = useState([]);
    const [start_date, setStartDate] = useState('');
    const [end_date, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMaxPresentations] = useState('');
    const [max_attendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.starts = start_date;
        data.ends = end_date;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log("newConference,",newConference);

            setName('');
            setStartDate("");
            setEndDate("");
            setDescription("");
            setMaxPresentations("");
            setMaxAttendees("");
            setLocation("");

        }

      }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

      const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
      }

      const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
      }

      const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
      }
      const handleMaxAttendeesChange= (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
      }
      const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
        console.log("description",description)
      }
      const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const locations_data = await response.json();
          setLocations(locations_data.locations)
          }
        }


      useEffect(() => {
        fetchData();
      }, []);

    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input placeholder="Name" onChange={handleNameChange} name="name" required type="text" id="name" className="form-control" value={name}/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Start Date" onChange={handleStartDateChange} name="starts" required type="date" id="start_date" value={start_date} className="form-control"/>
              <label htmlFor="start_date">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="End Date" onChange={handleEndDateChange} name="ends" required type="date" id="end_date" value={end_date} className="form-control"/>
              <label htmlFor="end_date">End Date</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" name="description" className="form-label">Description</label>
              <textarea className="form-control" id="description" rows="3" onChange={handleDescriptionChange}  value={description}></textarea>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Max Presentations" value={max_presentations} onChange={handleMaxPresentationsChange} name="max_presentations" required type="number" id="max_presentations" className="form-control"/>
              <label htmlFor="max_presentations">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Max Attendees" value={max_attendees} onChange={handleMaxAttendeesChange}  name="max_attendees" required type="number" id="max_attendees" className="form-control"/>
              <label htmlFor="max_attendees">Max Attendees</label>
            </div>
            <div className="mb-3">
              <select required id="location" value={location} onChange={handleLocationChange} name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                        return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default ConferenceForm;
