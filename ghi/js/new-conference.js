window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        const alertMessage = `Error: ${response.status} ${response.statusText}`;
        const alert = sendAlert(alertMessage);

        // Add the alert to your HTML
        const error = document.querySelector('#error-message');
        error.innerHTML = alert;
      } else {
        const data = await response.json();
        const selectTag = document.getElementById('location');
        console.log("data",data)

        for (let location of data.locations) {
          console.log("location",location)
          const option = document.createElement('option')
          option.value  = location.id
          console.log("optionvalue",option.value)
          option.innerHTML = location.name;
          selectTag.appendChild(option)
        }

      }
    } catch (e) {
      console.error(e);
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      console.log("fetch",fetchConfig)
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          console.log(newConference);
        }
      })
    });
