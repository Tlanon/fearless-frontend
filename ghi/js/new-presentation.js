window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';


    try {
      const conferenceResponse = await fetch(url);

      if (!conferenceResponse.ok) {
        const alertMessage = `Error: ${conferenceResponse.status} ${conferenceResponse.statusText}`;
        const alert = sendAlert(alertMessage);
        const error = document.querySelector('#error-message');
        error.innerHTML = alert;
      } else {
        const conferenceData = await conferenceResponse.json();
        console.log(conferenceData);
        const conferenceSelectTag = document.getElementById('conference');

        for (let conference of conferenceData.conferences) {
          const option = document.createElement('option')
          option.value  = conference.id
          option.innerHTML = conference.name;
          conferenceSelectTag.appendChild(option)
        }
      }
    } catch (e) {
      console.error(e);
    }

    const formTag = document.getElementById('create-presentation-form');
    console.log("formTag",formTag)
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const conferenceSelectedTag = document.getElementById('conference');
      const conferenceSelect = conferenceSelectedTag.value;
      console.log("conferenceSelect", conferenceSelect)
      if (conferenceSelect.selectedIndex === 0) {
        console.log('No conference selected.');
        return;
      }

      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(formTag.value)
      console.log('formData:', formData);
      console.log('json:', json);



      const presentationUrl = `http://localhost:8000/api/conferences/${conferenceSelect}/presentations/`;


      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(presentationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
      }
    })
});
